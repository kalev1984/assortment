using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.Base;

namespace Domain
{
    public class Division : DomainEntity
    {
        [MaxLength(50), MinLength(2)] public string? DivisionName { get; set; }

        [MaxLength(50), MinLength(2)] public string? DivisionCode { get; set; }

        public ICollection<Product>? Products { get; set; }
    }
}