﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.Base;

namespace Domain
{
    public class Product : DomainEntity
    {
        [MaxLength(200), MinLength(2)] public string ProductName { get; set; } = default!;

        [MaxLength(8), MinLength(8)] public string ProductCode { get; set; } = default!;

        [MaxLength(20), MinLength(2)] public string Brand { get; set; } = default!;

        [Range(999999999, 9999999999999)]
        public long Barcode { get; set; }

        //[ForeignKey(nameof(Division))] - in case of creating custom foreign key
        public Guid DivisionId { get; set; }
        public Division? Division { get; set; }

        //[InverseProperty(nameof(ProductInShop.Shop2))] - in case of having multiple products(in ProductInShop are multiple Shop objects)
        public ICollection<ProductInShop>? ProductInShops { get; set; }
    }
}