using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.Base;
using Domain.Identity;

namespace Domain
{
    public class Person : DomainEntity
    {
        [MaxLength(50), MinLength(2)] public string FirstName { get; set; } = default!;

        [MaxLength(50), MinLength(2)] public string LastName { get; set; } = default!;

        [Range(5000000, 60000000)]
        public int PhoneNumber { get; set; } = default!;
        
        public ICollection<Shop>? Shops { get; set; }

        public Guid AppUserId { get; set; }
        public AppUser? AppUser { get; set; }
    }
}