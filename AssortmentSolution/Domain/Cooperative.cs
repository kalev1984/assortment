using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.Base;

namespace Domain
{
    public class Cooperative : DomainEntity
    {
        [MaxLength(200), MinLength(2)] public string? CooperativeName { get; set; }

        public ICollection<Shop>? Shops { get; set; }
    }
}