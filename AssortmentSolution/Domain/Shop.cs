using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DAL.Base;

namespace Domain
{
    public class Shop : DomainEntity
    {
        [MaxLength(50), MinLength(2)] public string ShopName { get; set; } = default!;

        [MaxLength(50), MinLength(2)] public string ShopCode { get; set; } = default!;

        [MaxLength(100), MinLength(4)]
        public string ShopEmail { get; set; } = default!;

        public Guid ChainId { get; set; }
        public Chain? Chain { get; set; }

        public Guid PersonId { get; set; }
        public Person? Person { get; set; }

        public ICollection<ProductInShop>? ProductInShops { get; set; }
        
        public Guid CooperativeId { get; set; }
        public Cooperative? Cooperative { get; set; }
    }
}