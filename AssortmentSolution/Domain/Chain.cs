using System.ComponentModel.DataAnnotations;
using DAL.Base;

namespace Domain
{
    public class Chain : DomainEntity
    {
        [MaxLength(100), MinLength(2)] public string ChainName { get; set; } = default!;
    }
}