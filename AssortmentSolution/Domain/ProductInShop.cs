using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DAL.Base;

namespace Domain
{
    public class ProductInShop : DomainEntity
    {
        public Guid ShopId { get; set; }
        public Shop? Shop { get; set; }

        public Guid ProductId { get; set; }
        public Product? Product { get; set; }
    }
}