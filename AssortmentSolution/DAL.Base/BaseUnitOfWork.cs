using System;
using System.Collections.Generic;

namespace DAL.Base
{
    public class BaseUnitOfWork
    {
        private readonly Dictionary<Type,object> _repositoryCache = new Dictionary<Type,object>();
        
        protected TRepository GetRepository<TRepository>(Func<TRepository> repositoryCreationMethod) {
            
            if (_repositoryCache.TryGetValue(typeof(TRepository), out var repository))
            {
                return (TRepository) repository;
            }

            repository = repositoryCreationMethod()!;
            _repositoryCache.Add(typeof(TRepository), repository);
            return (TRepository) repository;
        }
    }
}