using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using Domain;

namespace Contracts.DAL.App.Repositories
{
    public interface IProductInShopRepository : IBaseRepository<ProductInShop>
    {
        Task<IEnumerable<ProductInShop>> GetAllProductsAsync(Guid id);
        List<ProductInShop> GetAllProducts(Guid id);
        Shop GetShop(Guid id);
    }
}