using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using Domain;

namespace Contracts.DAL.App.Repositories
{
    public interface IChainRepository : IBaseRepository<Chain>
    {
        Task<IEnumerable<Chain>> FindChainsAsync(string chainName);
    }
}