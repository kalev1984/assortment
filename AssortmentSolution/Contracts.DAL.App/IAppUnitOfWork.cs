using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IBaseUnitOfWork
    {
        IChainRepository Chains { get; }
        ICooperativeRepository Cooperatives { get; }
        IDivisionRepository Divisions { get; }
        IPersonRepository Persons { get; }
        IProductRepository Products { get; }
        IProductInShopRepository ProductInShops { get; }
        IShopRepository Shops { get; }
    }
}