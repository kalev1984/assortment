:: Drop database
dotnet ef database drop --project DAL.App.EF --startup-project WebApp

:: Remove migrations
dotnet ef migrations remove --project DAL.App.EF --startup-project WebApp

:: Create migrations
dotnet ef migrations add InitialMigration --project DAL.App.EF --startup-project WebApp

:: Update database
dotnet ef database update --project DAL.App.EF --startup-project WebApp