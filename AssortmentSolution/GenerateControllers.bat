:: Must be inside WebApp folder
cd WebApp

:: Create Web Controllers
dotnet aspnet-codegenerator controller -name ChainsController -actions -m Chain -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name CooperativesController -actions -m Cooperative -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name ProductsController -actions -m Product -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name ShopsController -actions -m Shop -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name DivisionsController -actions -m Division -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name PersonsController -actions -m Person -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name ProductInShopsController -actions -m ProductInShop -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f

:: Create API Controllers
:: dotnet aspnet-codegenerator controller -name DivisionsController -actions -m Division -dc AppDbContext -outDir ApiControllers -api --useAsyncActions -f
:: dotnet aspnet-codegenerator controller -name DocumentRegistriesController -actions -m DocumentRegistry -dc AppDbContext -outDir ApiControllers -api --useAsyncActions -f
:: dotnet aspnet-codegenerator controller -name PersonsController -actions -m Person -dc AppDbContext -outDir ApiControllers -api --useAsyncActions -f
:: dotnet aspnet-codegenerator controller -name VacationsController -actions -m Vacation -dc AppDbContext -outDir ApiControllers -api --useAsyncActions -f
:: dotnet aspnet-codegenerator controller -name VacationDaysController -actions -m VacationDay -dc AppDbContext -outDir ApiControllers -api --useAsyncActions -f
:: dotnet aspnet-codegenerator controller -name VacationTypesController -actions -m VacationType -dc AppDbContext -outDir ApiControllers -api --useAsyncActions -f