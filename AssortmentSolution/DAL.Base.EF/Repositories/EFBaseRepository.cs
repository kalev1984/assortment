﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.Base;
using Contracts.DAL.Base.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.Base.EF.Repositories
{
    public class EFBaseRepository<TEntity, TDbContext> : BaseRepository<TEntity, Guid, TDbContext> 
        where TEntity : class, IDomainEntity<Guid>, new()
        where TDbContext : DbContext
    {
        public EFBaseRepository(TDbContext dbContext) : base(dbContext)
        {
        }
    }
    
    public class BaseRepository<TEntity, TKey, TDbContext> : IBaseRepository<TEntity, TKey> 
        where TEntity : class, IDomainEntity<TKey>, new()
        where TKey : struct, IComparable
        where TDbContext : DbContext
    {
        protected TDbContext RepositoryDbContext;
        protected DbSet<TEntity> RepositoryDbSet;
        
        public BaseRepository(TDbContext dbContext)
        {
            RepositoryDbContext = dbContext;
            RepositoryDbSet = RepositoryDbContext.Set<TEntity>();

            if (RepositoryDbSet == null)
            {
                throw new ArgumentNullException(typeof(TEntity).Name + " was not found on DbSet");
            }
        }
        public virtual IEnumerable<TEntity> All()
        {
            return RepositoryDbSet.ToList();
        }

        public virtual async Task<IEnumerable<TEntity>> AllAsync()
        {
            return await RepositoryDbSet.ToListAsync();
        }

        public virtual TEntity Find(params object[] id)
        {
            return RepositoryDbSet.Find(id);
        }

        public virtual async Task<TEntity> FindAsync(params object[] id)
        {
            return await RepositoryDbSet.FindAsync(id);
        }

        public virtual TEntity Add(TEntity entity)
        {
            return RepositoryDbSet.Add(entity).Entity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            return RepositoryDbSet.Update(entity).Entity;
        }

        public virtual TEntity Remove(TEntity entity)
        {
            return RepositoryDbSet.Remove(entity).Entity;
        }

        public virtual TEntity Remove(params object[] id)
        {
            return Remove(RepositoryDbSet.Find(id));
        }

        public async Task<IEnumerable<TEntity>> Search(params object[] query)
        {
            return await RepositoryDbSet.ToListAsync();
        }
    }
}