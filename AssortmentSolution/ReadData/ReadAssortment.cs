using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Domain;
using Npgsql;

namespace ReadData
{
    public class ReadAssortment
    {
        private const string ConnectionString = "Server=kkilum.ddns.net;Port=5432;Database=ICD0009;User Id=kkilum;Password=Raudtee9;";
        private static readonly NpgsqlConnection Connection = new NpgsqlConnection(ConnectionString);
        
        public static void Assortment()
        {
            Connection.Open();

            List<ProductInShop> productInShops = new List<ProductInShop>();
            
            using StreamReader sr = new StreamReader(Environment.CurrentDirectory + "/assortment.csv", Encoding.UTF7);
            string currentLine;
            while((currentLine = sr.ReadLine()!) != null)
            {
                string[] strings = currentLine.Split(",");
                ProductInShop pis = new ProductInShop
                {
                    Id = Guid.NewGuid(),
                    ProductId = QueryProductId(strings[1]),
                    ShopId = QueryShopId(strings[0]),
                    CreatedBy = "Kalev Kilumets",
                    CreatedAt = DateTime.Now
                };
                productInShops.Add(pis);
                
                
            }

            for (var i = 0; i < productInShops.Count; i++)
            {
                var sql = "INSERT INTO public.\"ProductInShops\"(\"Id\",\"ProductId\", \"ShopId\", \"CreatedBy\", \"CreatedAt\") VALUES(@id, @productId, @shopId, @createdBy, @createdAt)";
                //        "INSERT INTO public.\"Products\"(\"Id\",\"ProductCode\", \"Barcode\", \"Brand\", \"ProductName\", \"CreatedBy\", \"CreatedAt\", \"DivisionId\") VALUES(@id, @productCode, @barCode, @brand, @productName, @createdBy, @createdAt, @divisionId)";
                using var cmd = new NpgsqlCommand(sql, Connection);
                cmd.Parameters.AddWithValue("id", productInShops[i].Id);
                cmd.Parameters.AddWithValue("productId", productInShops[i].ProductId);
                cmd.Parameters.AddWithValue("shopId", productInShops[i].ShopId);
                cmd.Parameters.AddWithValue("createdBy", productInShops[i].CreatedBy);
                cmd.Parameters.AddWithValue("createdAt", productInShops[i].CreatedAt);
                cmd.Prepare();
                cmd.ExecuteNonQuery();
            }
        }

        public static Guid QueryShopId(string shop)
        {
            Connection.Open();

            // Define a query returning a single row result set
            NpgsqlCommand command = new NpgsqlCommand("SELECT \"Id\" FROM public.\"Shops\" WHERE \"ShopName\" = '" + shop + "'", Connection);
            var count = (Guid)command.ExecuteScalar();
            Connection.Close();

            return count;
        }
        
        public static Guid QueryProductId(string productCode)
        {
            Connection.Open();

            // Define a query returning a single row result set
            NpgsqlCommand command = new NpgsqlCommand("SELECT \"Id\" FROM public.\"Products\" WHERE \"ProductCode\" = '" + productCode + "'", Connection);
            var count = (Guid)command.ExecuteScalar();
            Connection.Close();

            return count;
        }
    }
}