﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Domain;
using Npgsql;

namespace ReadData
{
    public class ReadProducts
    {
        public static void Products()
        {
            var cs = "Server=82.131.74.233;Port=5432;Database=ICD0009;User Id=kkilum;Password=Raudtee9;";
            using var con = new NpgsqlConnection(cs);
            con.Open();

            List<Product> products = new List<Product>();
            using StreamReader sr = new StreamReader(Environment.CurrentDirectory + "/products.csv");
            string currentLine;
            while((currentLine = sr.ReadLine()!) != null)
            {
                string[] strings = currentLine.Split(",");
                Product product = new Product
                {
                    Id = Guid.NewGuid(),
                    ProductCode = strings[0],
                    Barcode = Convert.ToInt64(strings[1]),
                    Brand = strings[2],
                    ProductName = strings[4],
                    CreatedBy = "Kalev Kilumets",
                    CreatedAt = DateTime.Now,
                    DivisionId = FindDivision(strings[3])
                };
                products.Add(product);
                
                
            }

            for (var i = 0; i < products.Count; i++)
            {
                var sql = "INSERT INTO public.\"Products\"(\"Id\",\"ProductCode\", \"Barcode\", \"Brand\", \"ProductName\", \"CreatedBy\", \"CreatedAt\", \"DivisionId\") VALUES(@id, @productCode, @barCode, @brand, @productName, @createdBy, @createdAt, @divisionId)";
                using var cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("id", products[i].Id);
                cmd.Parameters.AddWithValue("productCode", products[i].ProductCode);
                cmd.Parameters.AddWithValue("barCode", products[i].Barcode);
                cmd.Parameters.AddWithValue("brand", products[i].Brand);
                cmd.Parameters.AddWithValue("productName", products[i].ProductName);
                cmd.Parameters.AddWithValue("createdBy", products[i].CreatedBy);
                cmd.Parameters.AddWithValue("createdAt", products[i].CreatedAt);
                cmd.Parameters.AddWithValue("divisionId", products[i].DivisionId);
                cmd.Prepare();
                cmd.ExecuteNonQuery();
            }
            
        }

        private static Guid FindDivision(string Division)
        {
            if (Division.Equals("Dunker"))
            {
                return new Guid("f098d2f8-d5c4-403c-9bbe-32fdc9ab90d3");
            }
            if (Division.Equals("Dekanter"))
            {
                return new Guid("d3681684-b715-46c6-b6f8-8bdadeb683b5");
            }
            if (Division.Equals("Mediato"))
            {
                return new Guid("b1dfee3f-26f5-47aa-b249-6c941d01ab5b");
            } 
            throw new ArgumentException("Incorrect division name");
        }
    }
}   