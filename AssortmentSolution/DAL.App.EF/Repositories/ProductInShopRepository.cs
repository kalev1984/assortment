using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.Base.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class ProductInShopRepository : EFBaseRepository<ProductInShop, AppDbContext>, IProductInShopRepository
    {
        public ProductInShopRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public override IEnumerable<ProductInShop> All()
        {
            return RepositoryDbSet
                .Include(p => p.Product)
                .Include(p => p.Shop).ToList();
        }

        public override async Task<IEnumerable<ProductInShop>> AllAsync()
        {
            return await RepositoryDbSet
                .Include(p => p.Product)
                .Include(p => p.Shop).Take(10).ToListAsync();
        }

        public async Task<IEnumerable<ProductInShop>> GetAllProductsAsync(Guid id)
        {
            return await RepositoryDbSet.Where(p => p.ShopId == id)
                .Include(p => p.Product)
                .Include(p => p.Shop)
                .ToListAsync();
        }

        public List<ProductInShop> GetAllProducts(Guid id)
        {
            return RepositoryDbSet.Where(p => p.ShopId == id)
                .Include(p => p.Product)
                .Include(p => p.Shop)
                .ToList();
        }

        public Shop GetShop(Guid id)
        {
            Task<ProductInShop> pos = RepositoryDbSet.Where(s => s.ShopId == id).FirstOrDefaultAsync();
            return pos.Result.Shop!;
        }
    }
}