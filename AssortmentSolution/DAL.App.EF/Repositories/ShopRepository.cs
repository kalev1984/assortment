using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.Base.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class ShopRepository : EFBaseRepository<Shop, AppDbContext>, IShopRepository
    {
        public ShopRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<IEnumerable<Shop>> AllAsync()
        {
            return await RepositoryDbSet
                .Include(p => p.Chain)
                .Include(p => p.Person)
                .Include(p => p.Cooperative).Take(10).ToListAsync();
        }

        public override IEnumerable<Shop> All()
        {
            return RepositoryDbSet
                .Include(p => p.Chain)
                .Include(p => p.Person)
                .Include(p => p.Cooperative).ToList();
        }

        public async Task<IEnumerable<Shop>> FindShopsAsync(string query)
        {
            return await RepositoryDbSet.Where(p => p.ShopName.ToLower().Contains(query.ToLower())).ToListAsync();
        }
    }
}