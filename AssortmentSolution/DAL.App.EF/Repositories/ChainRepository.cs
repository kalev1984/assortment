using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.Base.EF.Repositories;
using Domain;
using Domain.Identity;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class ChainRepository : EFBaseRepository<Chain, AppDbContext>, IChainRepository
    {
        public ChainRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<IEnumerable<Chain>> AllAsync()
        {
            return await RepositoryDbSet.Where(p => p.DeletedAt!.Value == null).ToListAsync();
        }

        public override Chain Remove(params object[] id)
        {
            Chain chain = RepositoryDbSet.Find(id);
            chain.DeletedAt = DateTime.Now;
            chain.DeletedBy = "Kalev";
            return chain;
        }

        public async Task<IEnumerable<Chain>> FindChainsAsync(string chainName)
        {
            return await RepositoryDbSet.Where(p => p.ChainName.ToLower().Contains(chainName.ToLower())).ToListAsync();
        }

        //soft delete goes here:
        
    }
}