using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.Base.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class PersonRepository : EFBaseRepository<Person, AppDbContext>, IPersonRepository
    {
        public PersonRepository(AppDbContext dbContext) : base(dbContext)
        { 
        }

        public override async Task<IEnumerable<Person>> AllAsync()
        {
            return await RepositoryDbSet.Include(p => p.AppUser).ToListAsync();
        }

        public override IEnumerable<Person> All()
        {
            return RepositoryDbSet.Include(p => p.AppUser).ToList();
        }
    }
}