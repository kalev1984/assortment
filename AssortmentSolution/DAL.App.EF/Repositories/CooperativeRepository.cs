using Contracts.DAL.App.Repositories;
using DAL.Base.EF.Repositories;
using Domain;

namespace DAL.App.EF.Repositories
{
    public class CooperativeRepository : EFBaseRepository<Cooperative, AppDbContext>, ICooperativeRepository
    {
        public CooperativeRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}