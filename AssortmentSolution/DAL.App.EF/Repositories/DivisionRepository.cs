using Contracts.DAL.App.Repositories;
using DAL.Base.EF.Repositories;
using Domain;

namespace DAL.App.EF.Repositories
{
    public class DivisionRepository : EFBaseRepository<Division, AppDbContext>, IDivisionRepository
    {
        public DivisionRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}