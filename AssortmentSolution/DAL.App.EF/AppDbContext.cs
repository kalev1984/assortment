﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contracts.DAL.Base;
using Domain;
using Domain.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    public class AppDbContext : IdentityDbContext<AppUser, AppRole, Guid>
    {
        private IUsernameProvider _usernameProvider;
        public DbSet<Chain> Chains { get; set; } = default!;
        public DbSet<Cooperative> Cooperatives { get; set; } = default!;
        public DbSet<Division> Divisions { get; set; } = default!;
        public DbSet<Person> Persons { get; set; } = default!;
        public DbSet<Product> Products { get; set; } = default!;
        public DbSet<Shop> Shops { get; set; } = default!;
        public DbSet<ProductInShop> ProductInShops { get; set; } = default!;
        
        public AppDbContext(DbContextOptions<AppDbContext> options, IUsernameProvider usernameProvider) : base(options)
        {
            _usernameProvider = usernameProvider;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            foreach (var relationship in builder
                .Model.GetEntityTypes()
                .SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }

        private void SaveChangesMetaDataUpdate()
        {
            ChangeTracker.DetectChanges();

            var markedAsAdded = ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Added);

            foreach (var entityEntry in markedAsAdded)
            {
                if (!(entityEntry.Entity is IDomainEntityMetadata entityWithMetaData)) continue;
                entityWithMetaData.CreatedAt = DateTime.Now;
                entityWithMetaData.CreatedBy = _usernameProvider.CurrentUserName;
                entityWithMetaData.DeletedAt = entityWithMetaData.CreatedAt;
                entityWithMetaData.DeletedBy = entityWithMetaData.CreatedBy;
            }

            var markedAsModified = ChangeTracker.Entries().Where(x => x.State == EntityState.Modified);
            foreach (var entityEntry in markedAsModified)
            {
                // check for IDomainEntityMetadata
                if (!(entityEntry.Entity is IDomainEntityMetadata entityWithMetaData)) continue;

                // do not let changes on these properties get into generated db sentences - db keeps old values
                entityEntry.Property(nameof(entityWithMetaData.CreatedAt)).IsModified = false;
                entityEntry.Property(nameof(entityWithMetaData.CreatedBy)).IsModified = false;
            }
        }

        public override int SaveChanges()
        {
            SaveChangesMetaDataUpdate();
            return base.SaveChanges();
        }
        
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SaveChangesMetaDataUpdate();
            return base.SaveChangesAsync(cancellationToken);
        }

    }
}