using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Repositories;
using DAL.Base.EF;

namespace DAL.App.EF
{
    public class AppUnitOfWork : EFBaseUnitOfWork<AppDbContext>, IAppUnitOfWork
    {
        public AppUnitOfWork(AppDbContext uowDbContext) : base(uowDbContext)
        {
        }

        public IChainRepository Chains 
            => new ChainRepository(UOWDbContext);
        
        public ICooperativeRepository Cooperatives 
            => GetRepository<ICooperativeRepository>(() => new CooperativeRepository(UOWDbContext));
        
        public IDivisionRepository Divisions 
            => GetRepository<IDivisionRepository>(() => new DivisionRepository(UOWDbContext));
        
        public IPersonRepository Persons 
            => GetRepository<IPersonRepository>(() => new PersonRepository(UOWDbContext));  
        
        public IProductRepository Products 
            => GetRepository<IProductRepository>(() => new ProductRepository(UOWDbContext)); 
        
        public IProductInShopRepository ProductInShops 
            => GetRepository<IProductInShopRepository>(() => new ProductInShopRepository(UOWDbContext));  
        
        public IShopRepository Shops 
            => GetRepository<IShopRepository>(() => new ShopRepository(UOWDbContext));  
        
    }
}