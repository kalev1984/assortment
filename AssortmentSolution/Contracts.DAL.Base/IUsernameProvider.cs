namespace Contracts.DAL.Base
{
    public interface IUsernameProvider
    {
        public string CurrentUserName { get; }
    }
}