using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.ViewModels
{
    public class ProductInShopCreateEditViewModel
    {
        public ProductInShop ProductInShop { get; set; } = default!;

        public SelectList? ProductSelectList { get; set; }

        public SelectList? ShopSelectList { get; set; }
    }
}