using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.ViewModels
{
    public class ShopCreateEditViewModel
    {
        public Shop? Shop { get; set; }

        public SelectList? ChainSelectList { get; set; }

        public SelectList? CooperativeSelectList { get; set; }

        public SelectList? PersonSelectList { get; set; }

    }
}