using System;
using System.Collections.Generic;
using Domain;

namespace WebApp.ViewModels
{
    public class ProductInShopsCheckboxViewModel
    {
        public Shop Shop { get; set; } = null!;

        public List<CheckboxViewModel>? Checkboxes { get; set; } = new List<CheckboxViewModel>();
    }

    public class CheckboxViewModel
    {
        public bool IsSelected { get; set; }

        public string ProductName { get; set; } = default!;

        public long Barcode { get; set; }

        public Guid ProductId { get; set; }
    }
}