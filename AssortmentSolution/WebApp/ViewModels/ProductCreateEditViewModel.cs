using Domain;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.ViewModels
{
    public class ProductCreateEditViewModel
    {
        public Product Product { get; set; } = default!;

        public SelectList? DivisionSelectList { get; set; }
    }
}