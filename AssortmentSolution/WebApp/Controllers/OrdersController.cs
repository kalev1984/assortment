using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Domain;
using Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        private readonly IAppUnitOfWork _uow;
        
        public OrdersController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }
        
        [HttpPost]
        public Task<IActionResult> Assortment(Guid id, ProductInShopsCheckboxViewModel vm)
        {
            if (vm.Checkboxes!.Count == 0)
            {
                return Task.FromResult<IActionResult>(RedirectToAction(nameof(Index)));
            }
            
            string subject = "Puuduolevad kaubad: " + vm.Shop.ShopName;
            const string beginning = "Tere! \n\nJärgmised kaubad olid puudu: \n";
            Person person = _uow.Persons.FindAsync(vm.Shop.PersonId).Result;
            string end = "\n\nTervitades, \n" + person.FirstName + " " + person.LastName + "\n" + person.PhoneNumber;
            Console.WriteLine(end);
            string body;
            
            var fromAddress = new MailAddress(User.Identity.Name);
            var toAddress = new MailAddress(User.Identity.Name, "To Name");
            MailMessage message = new MailMessage(fromAddress, toAddress);
            message.Subject = subject;

            List<Product> products = new List<Product>();
            
            foreach (var cb in vm.Checkboxes)
            {
                if (!cb.IsSelected)
                {
                    Product product = 
                        new Product {Barcode = cb.Barcode, ProductName = cb.ProductName};
                    products.Add(product);
                }
            }
            
            if (products.Count == 0)
            {
                body = "Kõik tooted on olemas!";
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(beginning);
                foreach (var item in products)
                {
                    sb.Append(item.Barcode).Append("\t").Append(item.ProductName).Append("\n");
                }
                
                sb.Append(end);
                body = sb.ToString();
                if (vm.Shop.ShopEmail != User.Identity.Name)
                {
                    message.CC.Add(vm.Shop.ShopEmail);
                }
            }

            message.Body = body;
            
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("balmerkmail@gmail.com", "Raudtee9")
            };
            
            smtp.Send(message);
            return Task.FromResult<IActionResult>(RedirectToAction(nameof(Index)));
        }
        
        public async Task<IActionResult> Index(string? search)
        {
            if (string.IsNullOrWhiteSpace(search))
            {
                return View();
            }
            if (search.Equals("*"))
            {
                return View(await _uow.Shops.AllAsync());
            }
            return View(await _uow.Shops.FindShopsAsync(search));
        }

        public Task<IActionResult> Assortment(Guid id)
        {
            if (id == null)
            {
                return Task.FromResult<IActionResult>(NotFound());
            }
            
            IEnumerable<ProductInShop> products = _uow.ProductInShops.GetAllProducts(id);

            ProductInShopsCheckboxViewModel vm = new ProductInShopsCheckboxViewModel
            {
                Shop = _uow.ProductInShops.GetShop(id)
            };

            foreach (var p in products)
            {
                vm.Checkboxes!.Add(new CheckboxViewModel
                {
                    ProductName = p.Product!.ProductName,
                    Barcode = p.Product.Barcode,
                    ProductId = p.ProductId
                });
            }
            
            return Task.FromResult<IActionResult>(View(vm));
        }
    }
}