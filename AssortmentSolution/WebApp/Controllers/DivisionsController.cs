using System;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Mvc;
using Domain;
using Extensions;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    [Authorize]
    public class DivisionsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public DivisionsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Divisions
        public async Task<IActionResult> Index()
        {
            return View(await _uow.Divisions.AllAsync());
        }

        // GET: Divisions/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var division = await _uow.Divisions
                .FindAsync(id);
            if (division == null)
            {
                return NotFound();
            }

            return View(division);
        }

        // GET: Divisions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Divisions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DivisionName,DivisionCode,Id")] Division division)
        {
            division.CreatedBy = User.UserId();
            division.CreatedAt = DateTime.Now;
            if (ModelState.IsValid)
            {
                division.Id = Guid.NewGuid();
                _uow.Divisions.Add(division);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(division);
        }

        // GET: Divisions/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var division = await _uow.Divisions.FindAsync(id);
            if (division == null)
            {
                return NotFound();
            }
            return View(division);
        }

        // POST: Divisions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("DivisionName,DivisionCode,Id,CreatedBy,CreatedAt,DeletedBy,DeletedAt")] Division division)
        {
            division.CreatedBy = User.UserId();
            division.CreatedAt = DateTime.Now;
            if (id != division.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _uow.Divisions.Update(division);
                await _uow.SaveChangesAsync();
                
                return RedirectToAction(nameof(Index));
            }
            return View(division);
        }

        // GET: Divisions/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var division = await _uow.Divisions
                .FindAsync(id);
            if (division == null)
            {
                return NotFound();
            }

            return View(division);
        }

        // POST: Divisions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            _uow.Divisions.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
