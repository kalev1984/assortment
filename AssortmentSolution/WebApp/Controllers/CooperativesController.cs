using System;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Mvc;
using Domain;
using Extensions;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    [Authorize]
    public class CooperativesController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public CooperativesController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Cooperatives
        public async Task<IActionResult> Index()
        {
            return View(await _uow.Cooperatives.AllAsync());
        }

        // GET: Cooperatives/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cooperative = await _uow.Cooperatives.FindAsync(id);
            if (cooperative == null)
            {
                return NotFound();
            }

            return View(cooperative);
        }

        // GET: Cooperatives/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Cooperatives/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CooperativeName,Id")] Cooperative cooperative)
        {
            cooperative.CreatedBy = User.UserId();
            cooperative.CreatedAt = DateTime.Now;
            if (ModelState.IsValid)
            {
                cooperative.Id = Guid.NewGuid();
                _uow.Cooperatives.Add(cooperative);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cooperative);
        }

        // GET: Cooperatives/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cooperative = await _uow.Cooperatives.FindAsync(id);
            if (cooperative == null)
            {
                return NotFound();
            }
            return View(cooperative);
        }

        // POST: Cooperatives/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("CooperativeName,Id")] Cooperative cooperative)
        {
            cooperative.CreatedBy = User.UserId();
            cooperative.CreatedAt = DateTime.Now;
            if (id != cooperative.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _uow.Cooperatives.Update(cooperative);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cooperative);
        }

        // GET: Cooperatives/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cooperative = await _uow.Cooperatives.FindAsync(id);
            if (cooperative == null)
            {
                return NotFound();
            }

            return View(cooperative);
        }

        // POST: Cooperatives/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            _uow.Cooperatives.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
