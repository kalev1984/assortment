using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Microsoft.AspNetCore.Authorization;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    [Authorize]
    public class ShopsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public ShopsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Shops
        public async Task<IActionResult> Index()
        {
            return View(await _uow.Shops.AllAsync());
        }

        // GET: Shops/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shop = await _uow.Shops.FindAsync(id);
            if (shop == null)
            {
                return NotFound();
            }

            return View(shop);
        }

        // GET: Shops/Create
        public IActionResult Create()
        {
            var vm = new ShopCreateEditViewModel();
            vm.ChainSelectList = new SelectList(_uow.Chains.All(), nameof(Chain.Id), nameof(Chain.ChainName));
            vm.CooperativeSelectList = new SelectList(_uow.Cooperatives.All(), nameof(Cooperative.Id), nameof(Cooperative.CooperativeName));
            vm.PersonSelectList = new SelectList(_uow.Persons.All(), nameof(Person.Id), nameof(Person.FirstName));
            return View(vm);
        }

        // POST: Shops/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ShopCreateEditViewModel vm)
        {
            Console.WriteLine(vm.Shop!.ShopName);
            if (ModelState.IsValid)
            {
                _uow.Shops.Add(vm.Shop!);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            vm.ChainSelectList = new SelectList(await _uow.Chains.AllAsync(), nameof(Chain.Id), nameof(Chain.ChainName), vm.Shop!.ChainId);
            vm.CooperativeSelectList = new SelectList(await _uow.Cooperatives.AllAsync(), nameof(Cooperative.Id), nameof(Cooperative.CooperativeName), vm.Shop.CooperativeId);
            vm.PersonSelectList = new SelectList(await _uow.Persons.AllAsync(), nameof(Person.Id), nameof(Person.FirstName), vm.Shop.PersonId);
            return View(vm);
        }

        // GET: Shops/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shop = await _uow.Shops.FindAsync(id);
            if (shop == null)
            {
                return NotFound();
            }
            ShopCreateEditViewModel vm = new ShopCreateEditViewModel();
            vm.Shop = shop;
            vm.ChainSelectList = new SelectList(await _uow.Chains.AllAsync(), nameof(Chain.Id), nameof(Chain.ChainName));
            vm.CooperativeSelectList = new SelectList(await _uow.Cooperatives.AllAsync(), nameof(Cooperative.Id), nameof(Cooperative.CooperativeName));
            vm.PersonSelectList = new SelectList(await _uow.Persons.AllAsync(), nameof(Person.Id), nameof(Person.FirstName));
            return View(vm);
        }

        // POST: Shops/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, ShopCreateEditViewModel vm)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                vm.Shop!.Id = (Guid) id!;
                 _uow.Shops.Update(vm.Shop);
                 await _uow.SaveChangesAsync();
                 return RedirectToAction(nameof(Index));
            }
            vm.ChainSelectList = new SelectList(await _uow.Chains.AllAsync(), nameof(Chain.Id), nameof(Chain.ChainName), vm.Shop!.ChainId);
            vm.CooperativeSelectList = new SelectList(await _uow.Cooperatives.AllAsync(), nameof(Cooperative.Id), nameof(Cooperative.CooperativeName), vm.Shop.CooperativeId);
            vm.PersonSelectList = new SelectList(await _uow.Persons.AllAsync(), nameof(Person.Id), nameof(Person.FirstName), vm.Shop.PersonId);
            
            return View(vm);
        }

        // GET: Shops/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shop = await _uow.Shops.FindAsync(id);
            if (shop == null)
            {
                return NotFound();
            }

            return View(shop);
        }

        // POST: Shops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            _uow.Shops.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
