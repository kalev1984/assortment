using System;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Domain;
using Extensions;
using Microsoft.AspNetCore.Authorization;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    [Authorize]
    public class ProductInShopsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public ProductInShopsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: ProductInShops
        public async Task<IActionResult> Index()
        {
            return View(await _uow.ProductInShops.AllAsync());
        }

        // GET: ProductInShops/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productInShop = await _uow.ProductInShops.FindAsync(id);
            
            if (productInShop == null)
            {
                return NotFound();
            }

            return View(productInShop);
        }

        // GET: ProductInShops/Create
        public IActionResult Create()
        {
            ProductInShopCreateEditViewModel vm = new ProductInShopCreateEditViewModel();
            vm.ProductSelectList = new SelectList(_uow.Products.All(), nameof(Product.Id), nameof(Product.Brand));
            vm.ShopSelectList = new SelectList(_uow.Shops.All(), nameof(Shop.Id), nameof(Shop.ShopCode));
            return View(vm);
        }

        // POST: ProductInShops/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductInShopCreateEditViewModel vm)
        {
            if (ModelState.IsValid)
            {
                _uow.ProductInShops.Add(vm.ProductInShop!);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            vm.ProductSelectList = new SelectList(await _uow.Products.AllAsync(), nameof(Product.Id), nameof(Product.Brand), vm.ProductInShop!.ProductId);
            vm.ShopSelectList = new SelectList(await _uow.Shops.AllAsync(), nameof(Shop.Id), nameof(Shop.ShopCode), vm.ProductInShop.ShopId);
            return View(vm);
        }

        // GET: ProductInShops/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productInShop = await _uow.ProductInShops.FindAsync(id);
            if (productInShop == null)
            { 
                return NotFound();
            }

            ProductInShopCreateEditViewModel vm = new ProductInShopCreateEditViewModel();
            vm.ProductInShop = productInShop;
            vm.ProductSelectList = new SelectList(await _uow.Products.AllAsync(), nameof(Product.Id), nameof(Product.Brand), vm.ProductInShop!.ProductId);
            vm.ShopSelectList = new SelectList(await _uow.Shops.AllAsync(), nameof(Shop.Id), nameof(Shop.ShopCode), vm.ProductInShop.ShopId);
            return View(vm);
        }

        // POST: ProductInShops/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid? id, ProductInShopCreateEditViewModel vm)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                vm.ProductInShop!.Id = (Guid) id!;
                _uow.ProductInShops.Update(vm.ProductInShop);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            vm.ProductSelectList = new SelectList(await _uow.Products.AllAsync(), nameof(Product.Id), nameof(Product.Brand), vm.ProductInShop!.ProductId);
            vm.ShopSelectList = new SelectList(await _uow.Shops.AllAsync(), nameof(Shop.Id), nameof(Shop.ShopCode), vm.ProductInShop.ShopId);
            return View(vm);
        }

        // GET: ProductInShops/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productInShop = await _uow.ProductInShops.FindAsync(id);
            if (productInShop == null)
            {
                return NotFound();
            }

            return View(productInShop);
        }

        // POST: ProductInShops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            _uow.ProductInShops.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
