using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain;
using Extensions;
using Microsoft.AspNetCore.Authorization;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private readonly IAppUnitOfWork _uow;

        public ProductsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Products
        public async Task<IActionResult> Index()
        {
            return View(await _uow.Products.AllAsync());
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _uow.Products.FindAsync(id);
            
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public IActionResult Create()
        {
            var vm = new ProductCreateEditViewModel();
            vm.DivisionSelectList = new SelectList(_uow.Divisions.All(), nameof(Division.Id), nameof(Division.DivisionCode));
            return View(vm);
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductCreateEditViewModel vm)
        {
            vm.Product.CreatedAt = DateTime.Now;
            vm.Product.CreatedBy = User.UserId();
            if (ModelState.IsValid)
            {
                _uow.Products.Add(vm.Product!);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            vm.DivisionSelectList = new SelectList(await _uow.Divisions.AllAsync(), nameof(Division.Id), nameof(Division.DivisionCode), vm.Product!.DivisionId);
            return View(vm);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _uow.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            ProductCreateEditViewModel vm = new ProductCreateEditViewModel();
            vm.Product = product;
            vm.DivisionSelectList = new SelectList(await _uow.Divisions.AllAsync(), nameof(Division.Id), nameof(Division.DivisionCode));
            return View(vm);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProductCreateEditViewModel vm, Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                vm.Product.Id = (Guid) id!;
                _uow.Products.Update(vm.Product);
                await _uow.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            vm.DivisionSelectList = new SelectList(await _uow.Divisions.AllAsync(), nameof(Division.Id), nameof(Division.DivisionCode), vm.Product!.DivisionId);
            return View(vm);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _uow.Products.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            _uow.Products.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
