using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Mvc;
using Domain;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    [Authorize]
    public class ChainsController : Controller
    {
        private readonly IAppUnitOfWork _uow;
        
        public ChainsController(IAppUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Chains
        public async Task<IActionResult> Index(string? search)
        {
            //List<Chain> chains = new List<Chain>();  
            if (string.IsNullOrWhiteSpace(search))
            {
                return View(new List<Chain>());
            }
            return View(await _uow.Chains.FindChainsAsync(search));
        }

        // GET: Chains/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chain = await _uow.Chains.FindAsync(id);
            if (chain == null)
            {
                return NotFound();
            }

            return View(chain);
        }

        // GET: Chains/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Chains/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ChainName,Id,CreatedBy,CreatedAt,DeletedBy,DeletedAt")] Chain chain)
        {
            if (!ModelState.IsValid) return View(chain);
            chain.CreatedAt = DateTime.Now;
            chain.CreatedBy = User.Identity.Name;
            _uow.Chains.Add(chain);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Chains/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chain = await _uow.Chains.FindAsync(id);
            if (chain == null)
            {
                return NotFound();
            }
            return View(chain);
        }

        // POST: Chains/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("ChainName,Id,CreatedBy,CreatedAt,DeletedBy,DeletedAt")] Chain chain)
        {
            if (id != chain.Id)
            {
                return NotFound();
            }

            if (!ModelState.IsValid) return View(chain);
            _uow.Chains.Update(chain);
            await _uow.SaveChangesAsync();
                
            return RedirectToAction(nameof(Index));
        }

        // GET: Chains/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chain = await _uow.Chains.FindAsync(id);
            if (chain == null)
            {
                return NotFound();
            }

            return View(chain);
        }

        // POST: Chains/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            _uow.Chains.Remove(id);
            await _uow.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
